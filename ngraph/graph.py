from collections import defaultdict
class Graph(object):
    def __init__(self):
        self._connections = defaultdict(dict)

    def compute(self, node):
        # Get this node's inputs
        outputPlugs = self._connections[node]

        # Get the list of nodes those plugs belong to
        inputNodes = set([p.node for p in outputPlugs.values()])

        for inputNode in inputNodes:
            # Compute the input nodes
            if inputNode.dirty:
                node.dirty = True
            self.compute(inputNode)

        # Set the values of the connections
        for connectionPoint, plug in outputPlugs.iteritems():
            setattr(node, connectionPoint, plug())

        if node.dirty:
            node.compute()
        node.dirty = False

    def connect(self, source, target):
        self._connections[target.node][target.name] = source
