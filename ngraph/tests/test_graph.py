import pytest

from ngraph.node import Node
from ngraph.graph import Graph

class AddNode(Node):
    def __init__(self):
        super(AddNode, self).__init__()
        self._input1 = None
        self._input2 = None
        self._value = None

    @Node.plug
    def input1(self):
        '(int) The first input to the sum operation'
        return self._input1

    @input1.setter
    def input1(self, newValue):
        self._input1 = newValue
        self.dirty = True

    @Node.plug
    def input2(self):
        '(int) The second input to the sum operation'
        return self._input2

    @input2.setter
    def input2(self, newValue):
        self._input2 = newValue
        self.dirty = True

    @Node.plug
    def value(self):
        '(int) The sum of input1 and input2'
        return self._value

    @value.setter
    def value(self, newValue):
        self._value = newValue

    def compute(self):
        self.value = self.input1() + self.input2()

class IntNode(Node):
    def __init__(self, value):
        super(IntNode, self).__init__()
        self._value = value

    @Node.plug
    def value(self):
        '(int) The integer value stored by this Node'
        return self._value

    @value.setter
    def value(self, newValue):
        self._value = newValue
        self.dirty = True

class TestGraph(object):
    def test_compute(self):
        intNode5 = IntNode(5)
        intNode2 = IntNode(2)
        addNode = AddNode()
        addNode2 = AddNode()

        graph = Graph()
        graph.connect(intNode5.value, addNode.input1)
        graph.connect(intNode2.value, addNode.input2)
        graph.connect(addNode.value, addNode2.input1)
        graph.connect(intNode2.value, addNode2.input2)
        graph.compute(addNode2)

        assert addNode.value() == 7
        assert addNode2.value() == 9

    def test_dirty(self):
        intNode5 = IntNode(5)
        intNode2 = IntNode(2)
        addNode = AddNode()
        addNode2 = AddNode()

        graph = Graph()
        graph.connect(intNode5.value, addNode.input1)
        graph.connect(intNode2.value, addNode.input2)
        graph.connect(addNode.value, addNode2.input1)
        graph.connect(intNode2.value, addNode2.input2)
        graph.compute(addNode2)

        assert addNode.value() == 7
        assert addNode2.value() == 9

        intNode5.value = 0
        graph.compute(addNode2)
        assert addNode.value() == 2
        assert addNode2.value() == 4
