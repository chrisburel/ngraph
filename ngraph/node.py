class InstancePlug(object):
    def __init__(self, node, owner=None, fget=None, fset=None, fdel=None,
      doc=None):
        self._node = node
        self._type = owner
        self._get = fget
        self._set = fset
        self._del = fdel
        self.__doc__ = doc

    def __call__(self):
        '''
        Calling a plug returns the plug's value.
        '''
        return self._get.__get__(self._node, self._type)()

    @property
    def name(self):
        '(str) The name of the attribute this InstancePlug controls'
        return self._get.__name__

    @property
    def node(self):
        '(Node) The node instance this plug is attached to'
        return self._node

class Plug(object):
    '''
    An input or output on a Node.

    Plugs are bound to classes.  When getting an attribute on an instance,
    where the attribute is a Plug, an InstancePlug is returned.
    '''

    def __init__(self, fget=None, fset=None, fdel=None, doc=None):
        '''
        Create a new Plug instance.

        Args:
            fget (callable)
                The getter for this Plug
            fset (callable)
                The setter for this Plug
            fdel (callable)
                The deleter for this Plug
            doc (str)
                The docstring for this Plug
        '''
        self._get = fget
        self._set = fset
        self._del = fdel
        self.__doc__ = doc

    def __get__(self, instance, owner=None):
        '''
        Get the value of an attribute.

        Args:
            instance (object)
                The instance the attribute is being accessed through, or None
                if access is through the owing class.

            owner (type)
                The class that owns this Plug.

        Returns:
            The retrieved attribute

        Called to get the attribute of the owner class (class attribute access)
        or of an instance of that class (instance attribute access). owner is
        always the owner class, while instance is the instance that the
        attribute was accessed through, or None when the attribute is accessed
        through the owner. This method should return the (computed) attribute
        value or raise an AttributeError exception.
        '''

        if instance is None:
            # Class attribute access.  Return Plug instance.
            return self
        try:
            return instance.__instancePlugs[self.name]
        except (AttributeError, KeyError):
            if self._get is None:
                raise AttributeError('unreadable attribute')

            instancePlug = InstancePlug(
                instance,
                owner,
                self._get,
                self._set,
                self._del,
            )

            try:
                getattr(instance, '__instancePlugs')
            except AttributeError:
                instance.__instancePlugs = dict()
            instance.__instancePlugs[self.name] = instancePlug
            return instancePlug

    def __set__(self, instance, value):
        '''
        Set the attribute on `instance` of the owner class to `value`
        '''
        if self._set is None:
            raise AttributeError('can\'t set attribute')
        self._set(instance, value)

    @property
    def name(self):
        '(str) The attribute name of this Plug'
        return self._get.__name__

    def setter(self, fset):
        '''
        Change this Plug's setter function.

        Args:
            fset (callable):
                New setter function
        '''
        self._set = fset
        return self

class Node(object):
    '''
    A Node object represents a single piece of computation.

    Node subclasses provide classes to do actual work.  This class defines the
    common interface to all Node objects.
    '''

    @staticmethod
    def plug(method):
        '''
        This function decorator can be used like the property decorator to mark
        a method as being an output Plug for a Node.
        '''
        return Plug(method, doc=method.__doc__)

    def __init__(self):
        self._dirty = True

    @property
    def dirty(self):
        '(bool) True if this Node needs to be recomputed'
        return self._dirty

    @dirty.setter
    def dirty(self, isDirty):
        self._dirty = isDirty

    def compute(self):
        '''
        Compute this Node's outputs.

        The base implementation does nothing.
        '''
        pass

